<?php
include_once("$root/xtpl/xtemplate.class.php");

class News {
    function __construct() {}

    function index() {
        global $root;

        $xtpl = new XTemplate("$root/pages/news.html");
        $xtpl->assign("titre", $_SESSION["params"]["title"]);
        $xtpl->assign("id", $_SESSION["params"]["id"]);

        $xtpl->parse("main");
        $xtpl->out("main");
    }
}
?>
