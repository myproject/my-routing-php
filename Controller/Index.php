<?php
    include_once("$root/xtpl/xtemplate.class.php");

    class Index {
        function __construct() {}

        function index() {
            global $root;

            $xtpl = new XTemplate("$root/pages/index.html");
            $xtpl->assign("aboutLink", getLinkFromRouteName("about"));
            $xtpl->assign("connexionRouteLink", getLinkFromRoutePath("/connexion"));
            $xtpl->parse("main");
            $xtpl->out("main");
        }

        function connexion() {
            global $root;

            $xtpl = new XTemplate("$root/pages/connexion.html");
            $xtpl->assign("email", $_SESSION["params"]["email"]);
            $xtpl->assign("pass", $_SESSION["params"]["pass"]);
            $xtpl->parse("main");
            $xtpl->out("main");
        }

        function redirectToTest() {
            redirectFromRouteName("about");
        }
    }
?>