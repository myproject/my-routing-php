<?php

include_once("$root/xtpl/xtemplate.class.php");

class Article {
    function __construct() {}

    function index() {
        global $root;

        $xtpl = new XTemplate("$root/pages/article.html");
        $xtpl->assign("idArticle", $_SESSION["params"]["id"]);

        $xtpl->parse("main");
        $xtpl->out("main");
    }
}
?>
