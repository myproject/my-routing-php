<?php
    include_once("$root/xtpl/xtemplate.class.php");

    class About {
        function __construct() {}

        function about() {
            global $root;

            $xtpl = new XTemplate("$root/pages/about.html");
            $xtpl->parse("main");
            $xtpl->out("main");
        }
    }
?>