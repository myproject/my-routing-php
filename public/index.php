<?php
session_start();
include_once("../getRoot.php");

function getConfig() {
    global $root;
    return parse_ini_file("$root/config/config.ini", true)["config"];
}

function getRoutes() {
    global $root;
    return yaml_parse_file("$root/config/routes.yaml");
}

function getLinkFromRoutePath(String $route) {
    return (getConfig()["https"] ? "https://" : "http://") . $_SERVER['HTTP_HOST'] . $route;
}

function getLinkFromRouteName(String $name) {
    getRoutes();
    return (getConfig()["https"] ? "https://" : "http://") . $_SERVER['HTTP_HOST'] . getRoutes()[$name]["path"];
}

function redirectFromRoutePath(String $route) {
    header('Location: ' . $route);
    exit;
}

function redirectFromRouteName(String $name) {
    header('Location: ' . getRoutes()[$name]["path"]);
    exit;
}
function paramsIsGoodType(String $type, String $params) {
    $isGoodType = false;

    if(($type == "INT" || $type == "INTEGER") and is_numeric($params)) {
        $isGoodType = true;
    } else if (($type == "BOOL" || $type == "BOOLEAN") and in_array(strtoupper($params), ["TRUE", "FALSE"])) {
        $isGoodType = true;
    } else if (($type == "STR" || $type == "STRING") and is_string($params)) {
        $isGoodType = true;
    }

    return $isGoodType;
}

function trasnformParamToType(String $type, String $params) {
    $res = null;

    if(($type == "INT" || $type == "INTEGER")) {
        $res = intval($params);
    } else if (($type == "BOOL" || $type == "BOOLEAN")) {
        $res = strtoupper($params) == "TRUE" ? True : False;
    } else if (($type == "STR" || $type == "STRING")) {
        return $params;
    }

    return $res;
}

function isGoodRouteGET(String $path, string $url, array $type) : bool {
    $res = false;
    if (strpos($path, '?') !== false) {
        $isGoodRoute = true;
        $splitRoute = explode("/", $path);
        $splitRequest = explode("/", $url);
        if(sizeof($splitRequest) == sizeof($splitRoute)) {
            for($i = 0; $i < sizeof($splitRoute); ++$i) {
                if (strpos($splitRoute[$i], '?') !== false) {
                    if($splitRequest[$i] != "") {
                        $paramsName = str_replace("?", "", $splitRoute[$i]);
                        if(isset($type[$paramsName])) {
                            $type[$paramsName] = strtoupper($type[$paramsName]);
                            if(paramsIsGoodType($type[$paramsName], urldecode($splitRequest[$i]))) {
                                $_SESSION["params"][$paramsName] = trasnformParamToType($type[$paramsName], urldecode($splitRequest[$i]));
                            } else {
                                $isGoodRoute = false;
                            }
                        }
                        else {
                            $_SESSION["params"][$paramsName] = urldecode($splitRequest[$i]);
                        }
                    } else {
                        $isGoodRoute = false;
                    }
                } else {
                    if($splitRoute[$i] != $splitRequest[$i]) {
                        $isGoodRoute = false;
                    }
                }
            }
        } else {
            $isGoodRoute = false;
        }
        if($isGoodRoute) {
            $res = true;
        }
    } else if($url == $path) {
        $res = true;
    }

    return $res;
}

function isGoodRoutePOST(String $route, string $url, array $type) : bool {
    $res = false;

    if (strpos($route, '?') !== false) {
        $splitRoute = explode("/", $route);
        $argsRoute = [];
        $staticRoute = [];
        foreach($splitRoute as $routePart) {
            if(strpos($routePart, '?') !== false) {
                array_push($argsRoute, str_replace("?", "", $routePart));
            } else {
                array_push($staticRoute, $routePart);
            }
        }
        $splitUrl = explode("/", $url);

        $staticUrl = [];
        foreach($splitUrl as $urlPart) {
            if(!(strpos($urlPart, '?') !== false)) {
                array_push($staticUrl, $urlPart);
            }
        }

        $splitUrl = explode("/", $url);
        if(sizeof($splitUrl) == sizeof($staticRoute)) {
            $staticIsOrder = true;
            for($i = 0; $i < sizeof($staticRoute); ++$i) {
                if($staticRoute[$i] != $staticUrl[$i]) {
                    $staticIsOrder = false;
                    break;
                }
            }

            $hasAllArgs = true;
            foreach($argsRoute as $args) {
                if(!isset($_POST[$args])) {
                    $hasAllArgs = false;
                }
            }

            if($staticIsOrder && $hasAllArgs) {
                $isGoodRoute = true;
                foreach($argsRoute as $args) {
                    if(isset($type[$args])) {
                        $type[$args] = strtoupper($type[$args]);
                        if(paramsIsGoodType($type[$args], $_POST[$args])) {
                            $_SESSION["params"][$args] = trasnformParamToType($type[$args], $_POST[$args]);
                            unset($_POST[$args]);
                        } else {
                            $isGoodRoute = false;
                        }
                    } else {
                        $_SESSION["params"][$args] = $_POST[$args];
                        unset($_POST[$args]);
                    }
                }
            } else {
                $isGoodRoute = false;
            }
        } else {
            $isGoodRoute = false;
        }

        if($isGoodRoute) {
            $res = true;
        }

    } else if($url == $route) {
        $res = true;
    }

    return $res;
}

function main(String $url = null) {
    global $root;

    if($url == null) {
        $url = $_SERVER['REQUEST_URI'];
    }

    if(strlen($url) > 0 && $url != "/" && $url[strlen($url) - 1] == "/") {
        $url = substr($url, 0, -1);
    }

    $_SESSION["params"] = [];

    $foundRouteName = null;
    $routes = getRoutes();
    foreach($routes as $name => $settings) {
        if(isset($settings["method"])) {
            $method = strtoupper($settings["method"]);
        } else {
            $method = "GET";
        }

        $type = isset($settings["type"]) ? $settings["type"]: [];

        if($method == "GET") {
            if (isGoodRouteGET($settings["path"], $url, $type)) {
                $foundRouteName = $name;
                break;
            }
        } else if($method == "POST") {
            if(isGoodRoutePOST($settings["path"], $url, $type)) {
                $foundRouteName = $name;
                break;
            }
        }
    }

    if($foundRouteName == null) {
        $_SESSION["params"] = [];
        http_response_code(404);
        require_once($root."/404.php");
    } else {
        $file = explode("::", $routes[$foundRouteName]["controller"]);
        require_once($root . "/" . $file[0] . ".php");
        $explodePath = explode("/", $file[0]);
        $fileName = ucfirst($explodePath[sizeof($explodePath) - 1]);
        $object = new $fileName();
        $object->{$file[1]}();
    }
}

main();

?>