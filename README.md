#presentation

my routing php is simple groups of file to create easily website with routing.

## Installation
- import the file public, config and Controller folder at root of your project. (u can delete php file in controller and default route in routes.ini)

for windows:
- install yaml package: https://pecl.php.net/package/yaml
- set the file php_yaml.dll in: php/ext/php_yaml.dll
- to finish in your php.ini add extension=yaml

for linux:
- sudo apt update
- sudo apt install php<yourVersion>_yaml (replace <yourVersion> with your current php version)

## Usage
in config/routes.yaml you need in [routes] categorie set the name, path, controller and a method(if not define method = GET) for route like\
myRouteName:
  controller: myFolder/myController::myMethod
  method: GET
with that your create a route named 'myRouteName' when she call the myFolder/myController is call and de function myMethod.
you can use many route to one controller (ex: check de routes.yaml file in project).

you can set parameter to the url, ex:
myRouteName:
    path: /routePath/playerId?
    controller: myFolder/myController::myMethod
    method: GET

if the url is /routePath/5 you get in session a variable named 'params' with array 
with your player id. for last exemple to get id you need do:
PlayerId = $_SESSION["params"]["playerId"];<br/>
You can set a type for your variable in the yaml file like this:<br/>
type:<br/>
    playerId: int<br/>
all type are int, integer, str, string, bool and boolean.
if in the params playerId is not a int for user the routes is not found -> 404

for the controller you need to create in file a class with the file name with first letter in uppercase.
for exemple:
    controller: myFolder/myController::myMethod

the file myController has for content:
class MyController
{
    function myMethod()
    {
        // code
    }
}

## Contributing
no contributing for this moment... maybe later.

##caution
this project are not finish. they can get bug or security failure.